// Copyright Epic Games, Inc. All Rights Reserved.

#include "SkillBox_UE2_TDS.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, SkillBox_UE2_TDS, "SkillBox_UE2_TDS" );

DEFINE_LOG_CATEGORY(LogSkillBox_UE2_TDS)
 